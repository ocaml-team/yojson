Source: yojson
Section: ocaml
Priority: optional
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders: Julien Puydt <jpuydt@debian.org>
Build-Depends:
  debhelper-compat (= 13),
  ocaml-dune,
  ocaml,
  ocaml-best-compilers,
  ocaml-findlib,
  dh-ocaml (>= 1.2),
  debhelper (>= 12),
  libeasy-format-ocaml-dev,
  libalcotest-ocaml-dev <!nocheck>,
  libbiniou-ocaml-dev (>= 1.0.6)
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://github.com/ocaml-community/yojson
Vcs-Git: https://salsa.debian.org/ocaml-team/yojson.git
Vcs-Browser: https://salsa.debian.org/ocaml-team/yojson

Package: libyojson-ocaml-dev
Architecture: any
Depends: ${ocaml:Depends}, ${misc:Depends}, ${shlibs:Depends}
Provides: ${ocaml:Provides}
Recommends: ocaml-findlib
Description: JSON library for OCaml - development package
 Yojson is an optimized parsing and printing library for the JSON format.
 It addresses a few shortcomings of json-wheel including 3x speed
 improvement, polymorphic variants and optional syntax for tuples and
 variants.
 .
 It is a replacement for json-wheel (libjson-wheel-ocaml-dev).
 .
 This package contain the development files needed for programming
 with the library.

Package: libyojson-ocaml
Architecture: any
Depends: ${ocaml:Depends}, ${misc:Depends}, ${shlibs:Depends}
Provides: ${ocaml:Provides}
Recommends: ocaml-findlib
Description: JSON library for OCaml - runtime package
 Yojson is an optimized parsing and printing library for the JSON format.
 It addresses a few shortcomings of json-wheel including 3x speed
 improvement, polymorphic variants and optional syntax for tuples and
 variants.
 .
 It is a replacement for json-wheel (libjson-wheel-ocaml-dev).
 .
 This package contains the shared runtime libraries.

Package: yojson-tools
Architecture: any
Depends: ${ocaml:Depends}, ${misc:Depends}, ${shlibs:Depends}
Breaks: libyojson-ocaml-dev (<< 2.2)
Replaces: libyojson-ocaml-dev (<< 2.2)
Description: JSON library for OCaml - tools
 Yojson is an optimized parsing and printing library for the JSON format.
 It addresses a few shortcomings of json-wheel including 3x speed
 improvement, polymorphic variants and optional syntax for tuples and
 variants.
 .
 It is a replacement for json-wheel (libjson-wheel-ocaml-dev).
 .
 This package contains the tools.

